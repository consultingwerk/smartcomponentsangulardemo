import { BrowserModule } from '@angular/platform-browser';
import { AblDojoComponent } from './abl-dojo.component';
import { NgModule } from '@angular/core';
import { GridModule} from '@progress/kendo-angular-grid';
import { PanelBarModule } from '@progress/kendo-angular-layout';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AceEditorModule } from 'ng2-ace-editor';
// import { FileDropModule } from 'angular2-file-drop';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { NgxFileDropModule } from 'ngx-file-drop';
import { AblEditorComponent } from './abl-editor/abl-editor.component';

@NgModule({
  imports: [CommonModule, GridModule, PanelBarModule, FormsModule, NgxFileDropModule, AceEditorModule, InputsModule],
  declarations: [AblDojoComponent, AblEditorComponent],
  exports: [GridModule, PanelBarModule, AblDojoComponent, AblEditorComponent]
})
export class AblDojoModule {

}
