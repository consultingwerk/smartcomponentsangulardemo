import { Component, OnInit, Input, EventEmitter, Output, ViewChild, AfterViewInit } from '@angular/core';

@Component({
  selector: 'abl-editor',
  templateUrl: './abl-editor.component.html',
  styleUrls: ['./abl-editor.component.css']
})
export class AblEditorComponent implements OnInit, AfterViewInit {

  @Input() 
  public ablSource: string;

  @Output()
  public ablSourceChange = new EventEmitter<string>();

  @Output() 
  public ctrlXPressed = new EventEmitter<void>();

  @ViewChild('editor', { static: true }) editor;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.editor.setTheme('eclipse');

    this.editor.getEditor().setOptions({
      enableBasicAutocompletion: true,
      enableSnippets: false,
      enableLiveAutocompletion: true
    });

    this.editor.getEditor().commands.addCommand({
        name: 'showOtherCompletions',
        bindKey: 'Ctrl-X',
        exec: (editor) => {
          //this.executeAbl();
          this.ctrlXPressed.emit();
        }
    })
  }

  public ablSourceChanged() {
    this.ablSourceChange.emit(this.ablSource);
  }

}
