import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AblEditorComponent } from './abl-editor.component';

describe('AblEditorComponent', () => {
  let component: AblEditorComponent;
  let fixture: ComponentFixture<AblEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AblEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AblEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
