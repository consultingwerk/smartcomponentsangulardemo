import { Component, OnInit, AfterViewInit, ViewChild, Output } from '@angular/core';
import { SmartHttpService, SmartServiceAdapter } from "@consultingwerk/smartcomponent-library";
import { NgxFileDropEntry, FileSystemFileEntry } from 'ngx-file-drop';
@Component({
  selector: 'abl-dojo',
  templateUrl: './abl-dojo.component.html',
  styleUrls: ['./abl-dojo.component.css']
})
export class AblDojoComponent implements OnInit {

   ablSource: string = 'BLOCK-LEVEL ON ERROR UNDO, THROW.\n\n';
   ablResult: string = '';

  public fileIsOver: boolean = false;

  @Output() public options = {
    readAs: 'Text'
  };

  private file: File;


  constructor(public serviceAdapter: SmartServiceAdapter,
              private smartHttp: SmartHttpService) {

    const now = new Date();
    const hours = now.getHours();

    if (hours >= 4 && hours <= 9) {
      this.ablSource = this.ablSource +
        'MESSAGE "Good morning," Consultingwerk.Framework.Session.SessionManager:UserFullName SKIP(1). \n\n';
    }
    else {
      if (hours <= 17) {
        this.ablSource = this.ablSource +
          'MESSAGE "Good day," Consultingwerk.Framework.Session.SessionManager:UserFullName SKIP(1). \n\n';
      }
      else {
        if (hours <= 21) {
          this.ablSource = this.ablSource +
            'MESSAGE "Good evening," Consultingwerk.Framework.Session.SessionManager:UserFullName SKIP(1). \n\n';
        }
        else {
          this.ablSource = this.ablSource +
            'MESSAGE "Good night," Consultingwerk.Framework.Session.SessionManager:UserFullName SKIP(1). \n\n';
        }
      }
    }

    this.ablSource = this.ablSource + 'FIND FIRST Customer NO-LOCK .\n\n' +
                                      'Consultingwerk.Util.BufferHelper:ShowBuffer (BUFFER Customer:HANDLE). \n';
  }

  executeAbl () {

    this.smartHttp.post<string> (`${this.serviceAdapter.smartRestURI}/ExecuteAbl`, this.ablSource, { responseType: 'text' })
        .subscribe(response => {
            this.ablResult = response;
        });
  }

  ngOnInit() {
  }


  public onFileDrop(file: NgxFileDropEntry[]): void {
    this.fileIsOver = false;
    const reader = new FileReader();
    reader.onload = (ev: ProgressEvent) => {
      const content = (ev.target as FileReader).result;
      this.ablSource = content as string;
    }
    const fileEntry = file[0].fileEntry as FileSystemFileEntry;

    fileEntry.file(file => {
      reader.readAsText(file);
    })
  }
}
