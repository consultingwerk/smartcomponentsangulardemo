import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';

@Injectable()
export class LoadingStateService {
    public loadingState: ReplaySubject<boolean> = new ReplaySubject(1)

    constructor() { }

    public broadcastLoadingState(isLoading: boolean) { 
        this.loadingState.next(isLoading);
    }

}