import { Component, OnInit, Injector } from "@angular/core";
import { SmartCustomComponent, AbstractFormChild } from '@consultingwerk/smartcomponent-library';
import { LoadingStateService } from '../scheduler-loading.service';

@SmartCustomComponent('loadingIndicator')
@Component({
    selector: 'loading-indicator',
    templateUrl: './loading-indicator.component.html',
    styleUrls: ['./loading-indicator.component.css']
})
export class LoadingIndicatorComponent extends AbstractFormChild implements OnInit {
    public showAnimation: boolean = false;

    constructor(
        private loadingStateService: LoadingStateService,
        injector: Injector
    ) {
        super(injector);
    }

    ngOnInit() {
        this.loadingStateService.loadingState.subscribe(isLoading => this.showAnimation = isLoading)
    }

    handleLayoutChange(newLayout: { height: number; }) {
        
    }

}