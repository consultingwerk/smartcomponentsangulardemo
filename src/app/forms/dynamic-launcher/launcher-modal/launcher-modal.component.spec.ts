import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LauncherModalComponent } from './launcher-modal.component';

describe('LauncherModalComponent', () => {
  let component: LauncherModalComponent;
  let fixture: ComponentFixture<LauncherModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LauncherModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LauncherModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
