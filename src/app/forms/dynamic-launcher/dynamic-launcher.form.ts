import {
  SmartRouteGuard,
  SmartComponentLibraryModule,
  SmartFormComponent,
  CustomSmartForm,
  DataSourceRegistry,
  SmartViewerRegistryService,
  SmartTabFolderRegistryService,
  SmartViewManagerService,
  SmartFormInstanceService,
  SmartToolbarRegistry,
  SmartFilterRegistry,
  SmartDialogService,
  DialogButtons,
} from '@consultingwerk/smartcomponent-library';
import {
  Component,
  Injector,
  OnInit,
  OnDestroy,
  OnChanges,
  SimpleChanges,
  NgModule,
  ModuleWithProviders,
  ViewChild,
  AfterViewInit,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LauncherModalComponent } from './launcher-modal/launcher-modal.component';

@CustomSmartForm('dynamicLauncher')
@Component({
  selector: 'dynamic-launcher-form',
  templateUrl: './dynamic-launcher.form.html',
  styleUrls: ['./dynamic-launcher.form.css'],
  viewProviders: [
    DataSourceRegistry,
    SmartViewManagerService,
    SmartFormInstanceService,
    SmartToolbarRegistry,
    SmartViewerRegistryService,
    SmartTabFolderRegistryService,
    SmartFilterRegistry,
  ],
})
export class DynamicLauncherFormComponent extends SmartFormComponent
  implements OnInit, OnDestroy, OnChanges, AfterViewInit {
  @ViewChild('dynamicLauncherForm')
  dynamicLauncherForm: SmartFormComponent;

  selectedLayout: string;

  constructor(injector: Injector, private dialogService: SmartDialogService) {
    super(injector);
  }

  ngOnInit() {
    // Add your own initialization logic here

    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  ngOnChanges(changes: SimpleChanges) {
    super.ngOnChanges(changes);
  }

  ngAfterViewInit() {
    this.openDynamicLauncher();
  }

  openDynamicLauncher() {
    this.dialogService
      .showDialog({
        title: 'Dynamic Launcher',
        injector: this.injector,
        component: LauncherModalComponent,
        context: {
          selectedObjectMaster: '',
        },
        buttons: [DialogButtons.create('Select'), DialogButtons.CANCEL],
      })
      .then((result) => {
        const cmpInstance: LauncherModalComponent = result.componentInstance;
        if (
          !cmpInstance.selectedObjectMaster ||
          cmpInstance.selectedObjectMaster === '' ||
          result.button === DialogButtons.CANCEL
        ) {
          return;
        }
        this.selectedLayout = null;
        setTimeout(
          () =>
            (this.selectedLayout =
              cmpInstance.selectedObjectMaster &&
              `/SmartForm/Form/${cmpInstance.selectedObjectMaster}`)
        );
      });
  }
}

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: 'dynamic-launcher',
        component: DynamicLauncherFormComponent,
        canActivate: [SmartRouteGuard],
        outlet: 'view',
        data: {
          BreadcrumbLabelTemplate: 'Dynamic Launcher',
          BrowserTitleTemplate: 'Dynamic Launcher',
          FormId: 'dynamicLauncher',
        },
      },
    ]),
    SmartComponentLibraryModule,
  ],
  declarations: [DynamicLauncherFormComponent, LauncherModalComponent],
  entryComponents: [DynamicLauncherFormComponent, LauncherModalComponent],
  exports: [RouterModule],
})
export class DynamicLauncherFormModule {
  static entryComponents = [
    DynamicLauncherFormComponent,
    LauncherModalComponent,
  ];

  static forRoot(): ModuleWithProviders<DynamicLauncherFormModule> {
    return {
      ngModule: DynamicLauncherFormModule,
    };
  }
}
